# MySQL的表

建表语句为 ` ./order.sql `

先用最简单的表。集成进整个系统的时候再调整具体字段。。

# Redis的表

> 前缀： ORDER:PAYMENT:

使用最简单的字符串类型 k-v

key： 前缀+订单号

value：订单信息JSON串

# 本地缓存

使用spring提供的缓存抽象

- @Cacheable ： 根据方法的请求参数对其结果进行缓存。
- @CacheEvict：清空缓存
- @CachPut ：保证方法被调用，并更新缓存。