# redis安装和配置

## 虚拟机中安装

### 1.安装

- 下载安装包
https://redis.io/

- 解压
```shell
 tar -xvf redis-6.0.6.tar.gz
```

- 编译安装
```shell
 mv redis-6.0.6 redis
 cd redis
 make && make install
```

这里可能会有两个错误：

1. 在make&&make install时出现 make: *** [all] Error 2 。

   是gcc版本过低导致的，需要升级gcc。

   ```shell
   yum -y install centos-release-scl
   yum -y install devtoolset-9-gcc devtoolset-9-gcc-c++ devtoolset-9-binutils
   scl enable devtoolset-9 bash
   echo "source /opt/rh/devtoolset-9/enable" >> /etc/profile
   gcc -v
   ```

2. 执行make test时出现You need tcl 8.5 or newer in order to run the Redis test”

   ```shell
   wget
   http://downloads.sourceforge.net/tcl/tcl8.6.1-src.tar.gz  
   tar xzvf tcl8.6.1-src.tar.gz     
   cd  ./tcl8.6.1/unix/  
   ./configure  
   make  
   make install  
   ```

### 2.配置

修改安装目录下的redis.conf文件
```shell
vim redis.conf
```

修改以下配置：
```shell
#bind 127.0.0.1 # 将这行代码注释，监听所有的ip地址，外网可以访问
protected-mode no # 把yes改成no，允许外网访问
daemonize yes # 把no改成yes，后台运行
```

### 3.启动或停止

- redis-server 服务端命令，可以包含以下参数：
  start 启动
  stop 停止
- redis-cli 客户端控制台，包含参数：
  -h xxx 指定服务端地址，默认值是127.0.0.1
  -p xxx 指定服务端端口，默认值是6379

### 4.设置开机启动

1) 输入命令，新建文件

```sh
vim /etc/init.d/redis
```

注意：以下信息需要根据安装目录进行调整：

> EXEC=/usr/local/bin/redis-server # 执行脚本的地址
>
> REDIS_CLI=/usr/local/bin/redis-cli # 客户端执行脚本的地址
>
> PIDFILE=/var/run/redis.pid # 进程id文件地址
>
> CONF="/usr/local/src/redis-3.0.2/redis.conf" #配置文件地址

输入下面内容：

```sh
#!/bin/sh
# chkconfig:   2345 90 10
# description:  Redis is a persistent key-value database
PATH=/usr/local/bin:/sbin:/usr/bin:/bin

REDISPORT=6379
EXEC=/root/app/redis-6.0.6/src/redis-server  #看情况修改
REDIS_CLI=/root/app/redis-6.0.6/src/redis-cli  #看情况修改

PIDFILE=/var/run/redis_6379.pid  #看情况修改

CONF="/root/app/redis-6.0.6/redis.conf"  #看情况修改

case "$1" in  
    start)  
        if [ -f $PIDFILE ]  
        then  
                echo "$PIDFILE exists, process is already running or crashed"  
        else  
                echo "Starting Redis server..."  
                $EXEC $CONF  
        fi  
        if [ "$?"="0" ]   
        then  
              echo "Redis is running..."  
        fi  
        ;;  
    stop)  
        if [ ! -f $PIDFILE ]  
        then  
                echo "$PIDFILE does not exist, process is not running"  
        else  
                PID=$(cat $PIDFILE)  
                echo "Stopping ..."  
                $REDIS_CLI -p $REDISPORT SHUTDOWN  
                while [ -x ${PIDFILE} ]  
               do  
                    echo "Waiting for Redis to shutdown ..."  
                    sleep 1  
                done  
                echo "Redis stopped"  
        fi  
        ;;  
   restart|force-reload)  
        ${0} stop  
        ${0} start  
        ;;  
  *)  
    echo "Usage: /etc/init.d/redis {start|stop|restart|force-reload}" >&2  
        exit 1  
esac

```

然后保存退出

2）设置权限

```sh
chmod 755 /etc/init.d/redis
```

3）启动测试

```sh
/etc/init.d/redis start
```

启动成功会提示如下信息：

```
Starting Redis server...
Redis is running...
```

4）设置开机自启动

```sh
chkconfig --add /etc/init.d/redis
chkconfig redis on
```





### CentOS配置静态ip

1. 打开网卡配置 

   ```
   vim /etc/sysconfig/network-scripts/ifcfg-xxxxxxx
   ```

2. 修改配置

   ```shell
   BOOTPROTO=static        #开机协议，有dhcp及static；
   ONBOOT=yes              #设置为开机启动；
   DNS1=114.114.114.114    #这个是国内的DNS地址，是固定的；
   IPADDR=192.168.2.2      #你想要设置的固定IP，理论上192.168.2.2-255之间都可以，请自行验证；
   NETMASK=255.255.255.0   #子网掩码，不需要修改；
   GATEWAY=192.168.2.1     #网关，这里应该和你“2.配置虚拟机的NAT模式具体地址参数”中的（2）选择VMnet8--取消勾选使用本地DHCP--设置子网IP--网关IP设置 一样才行
   ```

   **注意： "="两边 千万不要加空格千万不要加空格千万不要加空格**

3. 重启网卡

   ```
   service network restart
   ```

### 打开防火墙端口

1. 查看防火墙状态

   ```shell
   # 查看服务状态
   systemctl status firewalld
   
   # 开启
   service firewalld start
   # 重启
   service firewalld restart
   # 关闭
   service firewalld stop
   
   # 查看防火墙规则
   firewall-cmd --list-all 
   ```

2. 开放端口

   ```shell
   # 查询端口是否开放
   firewall-cmd --query-port=8080/tcp
   # 开放80端口
   firewall-cmd --permanent --add-port=80/tcp
   # 移除端口
   firewall-cmd --permanent --remove-port=8080/tcp
   #重启防火墙(修改配置后要重启防火墙)firewall-cmd --reload
   ```

   

## docker中安装

1. 查询镜像

   ```
   docker search redis
   ```

2. 拉取官方镜像

   ```
   docker pull redis
   ```

3. 查看一下是否拉取成功

   ```
   docker images
   ```

4. 启动镜像

   ```shell
   docker run -p 6379:6379 -d redis:latest redis-server
   
   #持久化
   docker run -p 6379:6379 -v $PWD/data:/data  -d redis:3.2 redis-server --appendonly yes
   
   命令说明：
   -p 6379:6379 : 将容器的6379端口映射到主机的6379端口
   -v $PWD/data:/data : 将主机中当前目录下的data挂载到容器的/data
   redis-server --appendonly yes : 在容器执行redis-server启动命令，并打开redis持久化配置
   ```

5. 查看容器启动情况

   ```
   docker ps
   ```

6. 连接redis

   ```shell
   docker exec -ti d0b86 redis-cli
   
   docker exec -ti d0b86 redis-cli -h localhost -p 6379 
   docker exec -ti d0b86 redis-cli -h 127.0.0.1 -p 6379 
   docker exec -ti d0b86 redis-cli -h 172.17.0.3 -p 6379 
   # 注意，这个是容器运行的ip，可通过 docker inspect redis_s | grep IPAddress 查看
   docker exec -it redis_s redis-cli -h 192.168.1.100 -p 6379 -a your_password #如果有密码 使用 -a参数
   ```

   