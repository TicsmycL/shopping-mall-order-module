# RabbitMQ安装和配置

## 虚拟机中安装

### 1. 安装

1. 安装Erlang

   ```shell
   yum install esl-erlang_17.3-1~centos~6_amd64.rpm
   
   yum install esl-erlang-compat-R14B-1.el6.noarch.rpm
   ```

2. 安装RabbitMQ

   ```shell
   rpm -ivh rabbitmq-server-3.4.1-1.noarch.rpm
   ```

3. 设置配置文件

   ```shell
   cp /usr/share/doc/rabbitmq-server-3.4.1/rabbitmq.config.example /etc/rabbitmq/rabbitmq.config
   ```

4. 开启用于远程访问

 ![img](pic/clip_image008-1533941272259.png)

**注意要去掉后面的逗号。**

### 2. 启动、停止

```
service rabbitmq-server start

service rabbitmq-server stop

service rabbitmq-server restart
```

### 3. 开启web界面管理工具

```
rabbitmq-plugins enable rabbitmq_management

service rabbitmq-server restart
```

### 4. 设置开机启动

```
chkconfig rabbitmq-server on
```

### 5. 开放防火墙

```
/sbin/iptables -I INPUT -p tcp --dport 15672 -j ACCEPT

/etc/rc.d/init.d/iptables save
```



## docker中安装

1. 拉取带web管理界面的RabbitMQ镜像

   ```
   docker pull rabbitmq:management
   ```

2. 查看

   ```
   docker images
   ```

3. 运行

   ```shell
   docker run -d --name rabbitmq -p 5671:5671 -p 5672:5672 -p 4369:4369 -p 25672:25672 -p 15671:15671 -p 15672:15672 rabbitmq:management
   ```

4. 后台管理界面

   ```
   地址：http://IP:15672/ 账号密码：guest/guest
   ```

   