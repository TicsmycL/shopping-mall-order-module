package fun.ticsmyc.tmall.order_service.controller;

import fun.ticsmyc.tmall.order_service.bean.Order;
import fun.ticsmyc.tmall.order_service.service.OrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Ticsmyc
 * @date 2020-08-10 11:03
 */
@Api(tags = "订单管理")
@Controller
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;


    /**
     * 创建订单
     * @param order
     * @return
     */
    @ApiOperation(value = "创建订单", notes = "参数为订单信息的json数据，返回值为订单号")
    @PostMapping("create")
    public ResponseEntity<Long> createOrder(@RequestBody Order order){
        if(order == null){
            return ResponseEntity.badRequest().build();
        }
        Long orderId = this.orderService.createOrder(order);
        if(orderId == null){
            //order序列化失败
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(orderId);
    }


    /**
     * 完成付款，删除redis，持久化订单
     * @param id
     * @return
     */
    @ApiOperation(value = "完成付款后的回调函数")
    @PutMapping("pay/{orderId}")
    public ResponseEntity<Void> finishPayment(@PathVariable("orderId")Long id){
        if(id == null){
            return ResponseEntity.badRequest().build();
        }
        this.orderService.finishPayment(id);
        return ResponseEntity.ok().build();
    }

    /**
     * 取消未付款订单
     * @param id
     * @return
     */
    @ApiOperation(value = "取消未付款订单")
    @DeleteMapping("pay/{userId}/{orderId}")
    public ResponseEntity<Void> deleteOutstandingOrder(@PathVariable("orderId")Long id,@PathVariable("userId") String userId){
        if(id == null){
            return ResponseEntity.badRequest().build();
        }
        this.orderService.deleteOutstandingOrder(id,userId);
        return ResponseEntity.ok().build();
    }


    /**
     * 根据用户名查询用户订单。
     * 路径参数为用户id， 可以携带state参数
     * state=0 查询所有状态订单
     * state=1 查询未支付订单
     * state=2 查询已支付订单
     * @return
     */
    @ApiOperation(value = "根据用户名查询用户订单" , notes ="路径参数为用户id， 可以携带state参数\nstate=0 查询所有状态订单\n state=1 查询未支付订单\n state=2 查询已支付订单")
    @GetMapping("user/{userId}")
    public ResponseEntity<List<Order>> selectOrderByUserId(
            @PathVariable("userId")String userId,
            @RequestParam(value = "state", required = false, defaultValue = "0") Integer state

    ){
        if(userId == null){
            return ResponseEntity.badRequest().build();
        }
        List<Order> res = this.orderService.getOrderByUserId(userId,state);
        if(res == null || res.isEmpty()){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(res);
    }

    /**
     * 根据订单号查询订单
     * @param id
     * @return
     */
    @ApiOperation(value = "根据订单号查询订单")
    @GetMapping("{orderId}")
    public ResponseEntity<Order> getOrderFromMysql(@PathVariable("orderId")Long id){
        if(id == null){
            return ResponseEntity.badRequest().build();
        }
        Order order = this.orderService.getOrderByOrderId(id);
        if(order == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(order);
    }

}
