package fun.ticsmyc.tmall.order_service.bean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author Ticsmyc
 * @date 2020-08-10 10:54
 */
@Table(name="`order`")
@ApiModel(description = "订单类")
public class Order implements Serializable{

    /**
     * 订单号
     */
    @ApiModelProperty(value = "订单号", example = "1297729012138905600")
    @Id
    private Long id;
    /**
     * 商品名称
     */
    @ApiModelProperty(value = "商品名称")
    private String goodsName;
    /**
     * 商品描述
     */
    @ApiModelProperty(value = "商品描述")
    private String goodsDescription;
    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户id")
    private String userId;
    /**
     * 价格
     */
    @ApiModelProperty(value = "价格")
    private Double price;
    /**
     * 备注信息
     */
    @ApiModelProperty(value = "备注信息")
    private String remark;
    /**
     * 订单状态
     */
    @ApiModelProperty(value = "订单状态", example = "等待生成付款信息 或 待付款 或 已支付")
    private String state;
    /**
     * 支付信息
     */
    @ApiModelProperty(value = "支付信息")
    private String payment;

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", goodsName='" + goodsName + '\'' +
                ", goodsDescription='" + goodsDescription + '\'' +
                ", userId='" + userId + '\'' +
                ", price=" + price +
                ", remark='" + remark + '\'' +
                ", state='" + state + '\'' +
                ", payment='" + payment + '\'' +
                '}';
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsDescription() {
        return goodsDescription;
    }

    public void setGoodsDescription(String goodsDescription) {
        this.goodsDescription = goodsDescription;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }
}
