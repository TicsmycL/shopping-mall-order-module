package fun.ticsmyc.tmall.order_service.mapper;

import fun.ticsmyc.tmall.order_service.bean.Order;
import tk.mybatis.mapper.common.Mapper;


/**
 * @author Ticsmyc
 * @date 2020-08-10 10:56
 */

@org.apache.ibatis.annotations.Mapper
public interface OrderMapper extends Mapper<Order> {

}
