package fun.ticsmyc.tmall.order_service.config;

import fun.ticsmyc.tmall.order_service.bean.Order;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.net.UnknownHostException;

/**
 * 自定义用于Order类的redis客户端
 * @author Ticsmyc
 * @date 2020-08-19 15:53
 */
@Configuration
public class OrderRedisConfig {

    @Bean("orderRedisTemplate")
    public RedisTemplate<String, Order> orderRedisTemplate(RedisConnectionFactory redisConnectionFactory)
            throws UnknownHostException {
        RedisTemplate<String, Order> template = new RedisTemplate<>();
        template.setConnectionFactory(redisConnectionFactory);
        //使用Jackson作为序列化器
        template.setDefaultSerializer(new Jackson2JsonRedisSerializer<Order>(Order.class));
        template.setKeySerializer(new StringRedisSerializer());
        return template;
    }
}
